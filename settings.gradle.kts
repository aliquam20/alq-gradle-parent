rootProject.name = "alq-gradle-parent"

pluginManagement {
    repositories {
        mavenLocal()
        maven(url = "https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}
