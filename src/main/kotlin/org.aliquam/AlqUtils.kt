package org.aliquam

import org.gradle.api.Project
import org.gradle.api.publish.PublishingExtension
import java.net.URI

class AlqUtils(val project: Project) {
    private val DEFAULT_NEXUS_URL: String = "https://nexus.jeikobu.net/repository/maven-aliquam/"
    private val CONFIG_ALQ_ENV_KEYS: Array<String> = arrayOf("alqEnv", "ALQ_ENV")
    private val CONFIG_NEXUS_URL_KEY: String = "alqNexusUrl"
    private val CONFIG_NEXUS_USERNAME_KEYS: Array<String> = arrayOf("alqNexusUsername", "nexusUsername", "NEXUS_USERNAME")
    private val CONFIG_NEXUS_PASSWORD_KEYS: Array<String> = arrayOf("alqNexusPassword", "nexusPassword", "NEXUS_PASSWORD")

    fun withStandardProjectSetup(): AlqUtils {
        val repositories = project.repositories
        repositories.add(repositories.mavenLocal())
        repositories.add(repositories.maven { repo ->
            repo.url = URI(getNexusUrl())
            repo.credentials.username = getNexusUsernameOrThrow()
            repo.credentials.password = getNexusPasswordOrThrow()
        })
        repositories.add(repositories.mavenCentral())

        project.pluginManager.withPlugin("maven-publish") {
            project.extensions.configure(PublishingExtension::class.java) { publishing ->
                publishing.repositories.add(publishing.repositories.maven { repo ->
                    repo.url = URI(getNexusUrl())
                    repo.credentials.username = getNexusUsernameOrThrow()
                    repo.credentials.password = getNexusPasswordOrThrow()
                })
            }
        }

        return this
    }

    fun getEnvOrPropertyOrThrow(vararg keys: String): String {
        return getEnvOrProperty(*keys)
                ?: throw IllegalStateException("Missing environment variable or project property: '${keys}'")
    }

    fun getEnvOrProperty(vararg keys: String): String? {
        for(key in keys) {
            when {
                System.getenv(key) != null -> return System.getenv(key)
                project.hasProperty(key) -> return project.property(key).toString()
            }
        }
        return null
    }

    fun getSemVersion(baseVersion: String): String {
        // These variables are provided by Jenkins
        val branchName: String? = System.getenv("BRANCH_NAME")
        val buildNumber: String? = System.getenv("BUILD_NUMBER")

        return if (branchName != null && buildNumber != null) {
            when (branchName) {
                "develop" -> "$baseVersion-SNAPSHOT-$buildNumber"
                else -> baseVersion
            }
        } else {
            "$baseVersion-DEV_BUILD"
        }
    }

    fun getAlqEnvOrThrow(): String = getEnvOrPropertyOrThrow(*CONFIG_ALQ_ENV_KEYS)
    fun getNexusUrl(): String = getEnvOrProperty(CONFIG_NEXUS_URL_KEY) ?: DEFAULT_NEXUS_URL
    fun getNexusUsernameOrThrow(): String = getEnvOrPropertyOrThrow(*CONFIG_NEXUS_USERNAME_KEYS)
    fun getNexusPasswordOrThrow(): String = getEnvOrPropertyOrThrow(*CONFIG_NEXUS_PASSWORD_KEYS)
}
