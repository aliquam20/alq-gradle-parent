import java.net.URI
import org.aliquam.AlqUtils

plugins {
    `java-gradle-plugin`
    `maven-publish`
    id("org.jetbrains.kotlin.jvm") version "1.3.72"
    id("org.aliquam.alq-gradle-parent") version "0.4.12"
}
val alq = AlqUtils(project)

val BASE_VERSION = "0.4.13"

group = "org.aliquam"
version = alq.getSemVersion(BASE_VERSION)

repositories {
    mavenCentral()
}

dependencies {
    // Nothing so far
}

publishing {
    repositories {
        maven {
            url = URI("https://nexus.jeikobu.net/repository/maven-releases/")
            credentials {
                username = alq.getEnvOrPropertyOrThrow("alqNexusUsername", "nexusUsername", "NEXUS_USERNAME")
                password = alq.getEnvOrPropertyOrThrow("alqNexusPassword", "nexusPassword", "NEXUS_PASSWORD")
            }
        }
    }
}

gradlePlugin {
    plugins {
        create("alqGradleParentPlugin") {
            id = "org.aliquam.alq-gradle-parent"
            implementationClass = "org.aliquam.AlqGradleParentPlugin"
        }
    }
}
